local addonName = ...
local addonTitle = select(2, GetAddOnInfo(addonName))
ToGo = LibStub("AceAddon-3.0"):NewAddon(addonName, "AceConsole-3.0", "AceEvent-3.0", "AceTimer-3.0")

-- Local variables init
local StartRep = 0;
local StartTime = 0;

-- DB Defaults
local defaults = {
    profile = {
    },
    realm = {
        faction = nil
    },
    char = {
        startoutput = false,
        faction = nil,
        ["Alterac Valley"] = {},
        ["Warsong Gulch"] = {},
        ["Arathi Basin"] = {}
    }
}

-- ACE3 Config
local options = {
    name = "ToGo",
    handler = ToGo,
    type = "group",
    args = {
        startoutput = {
            name = "BG Start Message",
            desc = "Prints a basic message at the start of a BG.",
            type = "toggle",
            set = function(info,val) ToGo.db.char.startoutput = val end,
            get = function(info) return ToGo.db.char.startoutput end
        },
        endoutput = {
            name = "BG End Message",
            desc = "Prints a basic message at the conclusion of a BG.",
            type = "toggle",
            set = function(info,val) ToGo.db.char.endoutput = val end,
            get = function(info) return ToGo.db.char.endoutput end
        }
    }

}
local optionstable = LibStub("AceConfig-3.0"):RegisterOptionsTable(addonName, options, nil)
local AceConfigDialog = LibStub("AceConfigDialog-3.0")

--TOGO FUNCTIONS
function ToGo:newGame(date, rep, duration, win)
    local minutes = duration/60
    return {["date"] = date,  ["rep"] = rep, ["duration"] = minutes, ["repmin"] = rep/minutes, ["win"] = win}
end

function ToGo:GetBGFaction(zone)
	local allianceFactions = {["Alterac Valley"] = 730, ["Warsong Gulch"] = 890, ["Arathi Basin"] = 509};
    local hordeFactions = {["Alterac Valley"] = 729, ["Warsong Gulch"] = 889, ["Arathi Basin"] = 510};

    if ToGo.db.realm.faction == 0 then
        return allianceFactions[zone]
    else
        return hordeFactions[zone]
    end
end

function ToGo:CalculateGames(factionbg, goal)
    local gamehistory = ToGo.db.char[factionbg]
    if gamehistory == nil then return nil end
    local totalgames = #gamehistory
    local totalrep = 0
    local wins = 0

    if totalgames == 0 then return nil end

    for _, v in ipairs(ToGo.db.char[factionbg]) do
        totalrep = totalrep + v.rep
        wins = wins + v.win
    end

    local averageRep = totalrep / totalgames;
    local winrate = wins / totalgames;

    return (3*goal)/(3*averageRep + 200*winrate + 100), averageRep, totalgames, wins, winrate
end

function ToGo:CalculateTimes(faction, goal)
    local gamehistory = ToGo.db.char[faction]
    if table.getn(gamehistory) == 0 then return nil end

    local totalgames = #gamehistory
    local totalrepmin = 0
    local totalduration = 0
    local wins = 0

    if totalgames == 0 then return nil end

    for _, v in ipairs(gamehistory) do
        totalrepmin = totalrepmin + v.repmin
        totalduration = totalduration + v.duration
        wins = wins + v.win
    end

    local averagerepmin = totalrepmin / totalgames
    local averageduration = totalduration / totalgames
    local winrate = wins / totalgames;

    return (3*averageduration*goal)/((3*averageduration*averagerepmin)+200*winrate+100), averagerepmin, averageduration, totalduration, wins, winrate
end

--REQUIRED ACE3 FUNCTIONS
function ToGo:OnInitialize()
-- Set settings load once AceConfig integrated
    self.db = LibStub("AceDB-3.0"):New("ToGoSaved", defaults)
    AceConfigDialog:AddToBlizOptions(addonName, addonTitle)
    if self.db.realm.faction == nil then
        if UnitFactionGroup("player") == "Alliance" then self.db.realm.faction = 0 else self.db.realm.faction = 1 end
    end
    self.pvp = false
end

function ToGo:OnEnable()

end

function ToGo:OnDisable()
end

--SLASH COMMAND FUNCTIONS
function ToGo:ToGoArgParse(args)
    local arg1, arg2 = ToGo:GetArgs(args, 2)
    local argslist = {arg1, arg2}
    local toPrint = {}
    local verbose = false
	
	for _, v in ipairs(argslist) do
		if v == "av" then table.insert(toPrint, "Alterac Valley")
		elseif v == "ab" then table.insert(toPrint, "Arathi Basin")
		elseif v == "wb" then table.insert(toPrint, "Warsong Gulch")
		elseif v == "v" then verbose = true end
    end

    if #toPrint == 0 then 
		table.insert(toPrint, "Alterac Valley")
		table.insert(toPrint, "Arathi Basin")
		table.insert(toPrint, "Warsong Gulch")
	end
    
    return toPrint, verbose
end

function ToGo:GamesToGo(args)
    local function printgame(factionbg, verbose)
        local factionName, _, _, _, _, barval = GetFactionInfoByID(ToGo:GetBGFaction(factionbg))
        local gamesleft, averagerep, totalgames, wins, winrate = ToGo:CalculateGames(factionbg, 42000 - barval)
        
        if gamesleft == nil then
            self.Printf(self, "|cFFF08080No games recorded for |cFF008000%s.", factionName)
        else
            self.Printf(self,"|cFFF08080To reach exalted with |cFF008000%s, |cFFF08080will require %.0f more games.", factionName, math.floor(gamesleft)+1)
            if verbose == true then		
                self.Printf(self,"|cFFF08080Average rep for |cFF008000%s in |cFF008000%s |cFFF08080is |cFF008000%0.f.", factionName, factionbg, averagerep)
                self.Printf(self,"|cFFF08080You have played a total of |cFF008000%.0f %s |cFFF08080games, and won |cFF008000%.0f |cFFF08080of these games. Your win rate is |cFF008000%.2f.", totalgames, factionbg, wins, winrate)
            end
        end      
    end

    local toPrint, verbose = ToGo:ToGoArgParse(args)
    self.Printf(self, "|cFF008000=========================================================")
    for _, faction in ipairs(toPrint) do
        printgame(faction, verbose)
    end
    self.Printf(self, "|cFF008000=========================================================")
end

function ToGo:TimeToGo(args)
    local function printgame(factionbg, verbose)
        local factionName, _, _, _, _, barval = GetFactionInfoByID(ToGo:GetBGFaction(factionbg))
        local timeleft, averagerepmin, totalduration, _= ToGo:CalculateTimes(factionbg, 42000 - barval)
        
        if timeleft == nil then
            self.Printf(self, "|cFFF08080No games recorded for |cFF008000%s.", factionName)
        else
            self.Printf(self,"|cFFF08080To reach exalted with |cFF008000%s, |cFFF08080will require %.1f more minutes.", factionName, timeleft)
            if verbose == true then		
                self.Printf(self,"|cFFF08080Average rep per minute for |cFF008000%s in |cFF008000%s |cFFF08080is |cFF008000%0.f.", factionName, factionbg, averagerepmin)
                self.Printf(self,"|cFFF08080You have played a total of |cFF008000%1.f |cFFF08080in |cFF008000%s |cFFF08080.", totalduration, factionbg)
            end
        end
    end
    
    local toPrint, verbose = ToGo:ToGoArgParse(args)

    self.Printf(self, "|cFF008000=========================================================")
    for _, faction in ipairs(toPrint) do
        printgame(faction, verbose)
    end
    self.Printf(self, "|cFF008000=========================================================")
end

function ToGo:Test()
    table.insert(self.db.char["Arathi Basin"], ToGo:newGame(GetServerTime(), 100, 600, 0))
end

function ToGo:TestO()
    for k, v in ipairs(self.db.char["Arathi Basin"]) do
        print(k)
        print(v.date)
        print(v.rep)
        print(v.duration)
        print(v.repmin)
        print(v.win)
    end
end

function ToGo:TestC()
    self.db.char["Arathi Basin"] = {}
end

ToGo:RegisterChatCommand("test", "Test")
ToGo:RegisterChatCommand("testo", "TestO")
ToGo:RegisterChatCommand("testc", "TestC")
ToGo:RegisterChatCommand("tgg", "GamesToGo")
ToGo:RegisterChatCommand("tgt", "TimeToGo")

--EVENTS
function ToGo:EnterTimerResponse()
    self.bg = ToGo:GetBGFaction(GetRealZoneText())
    self.timercount = self.timercount + 1
	print(self.timercount)
    if self.timercount == 4 or not(self.bg == nil) then self:CancelTimer(self.entertimer) end
    if self.bg == nil then return nil end

    self.pvp = true
    local name, _,  _, _, barmax, barval = GetFactionInfoByID(self.bg);
    StartRep = barval;
    StartTime = GetServerTime();
    if ToGo.db.char.startoutput == true then
        print("BG INFO:");
        print("Name: ", name);
        print("Rep: ", barval);
    end
end

function ToGo:PLAYER_ENTERING_BATTLEGROUND()
	self.timercount = 0
    self.entertimer = self:ScheduleRepeatingTimer("EnterTimerResponse", 15)
end

function ToGo:PLAYER_ENTERING_WORLD()   
    self.pvp = false
end

function ToGo:CHAT_MSG_LOOT(...) 
    local _, msg = ...
    if string.match(msg, "Mark of Honor") then
        if string.match(msg, "You create") then
            pvp = 0
			local bg = GetRealZoneText();
			local name, _, _, _, barmax, barval = GetFactionInfoByID(ToGo:GetBGFaction(GetRealZoneText()))
            local endRep = barval;
            local duration = GetServerTime() - StartTime;
            local win = 0
			
			if string.match(msg, "3") then 
                win = 1
            end

            table.insert(self.db.char[bg], ToGo:newGame(StartTime, endRep-StartRep, duration, win))
            
            if ToGo.db.char.endoutput == true then
                print("Avgs: ", endRep - StartRep); 
                print("Game Rep: ", endRep - StartRep);
                print("Wins: ", win)
            end
       end
    end
end

ToGo:RegisterEvent("PLAYER_ENTERING_BATTLEGROUND")
ToGo:RegisterEvent("PLAYER_ENTERING_WORLD")
ToGo:RegisterEvent("CHAT_MSG_LOOT")